<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '010_af_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'yourpassword' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'SK7CyP2T)k9w8T(CU04)!C5g&+f(#`uq-(sahFU/#)=q&IP]!9,}C:6yi/n1 vV@' );
define( 'SECURE_AUTH_KEY',   '/oMwt`LEWk)s>!(Pm)WU-[g%+dGRsUQ?s5~<YArL` 4:J6b|mzdmuZChH5PFI(8;' );
define( 'LOGGED_IN_KEY',     'eg|t{]P+&fx.xwJ%$7h_X Q!iV]}r/>3>Told[R1_gZ~B*7KZ2rL?!5MSP1 UzG;' );
define( 'NONCE_KEY',         '/P~Y8<rG_]*y5aM~&V@34f+J54MO;Y)e9X1*}z0h7mKSPQ/wJ3i(@i6V>GjnEMEb' );
define( 'AUTH_SALT',         'c2m>#[Iv,:5czPJ1bBQH4[$S:fMF9t)C;X@kg8rJ:GW7frLr,dB*4P#vo.g]qc-K' );
define( 'SECURE_AUTH_SALT',  '$k$%mhtd3~Wg=.ocP8|@-=i(umc> *,Z1^4P{dPPN)@FBwyA?!9_f^{eP #H5hG5' );
define( 'LOGGED_IN_SALT',    'Fd[%PC?wF1PyCF:Sw+o<G3k.V?/C>bB*WnO9[<yF6vs=#PcV3I/q QlQuLam{fhC' );
define( 'NONCE_SALT',        'lb8@K*?s]g7#WBr[{WHV>yXv~lH`sSZ+3kA t3($oI;VR /)0*D|^O|j!ctKRO9H' );
define( 'WP_CACHE_KEY_SALT', '@=Z(_47revgxAi8]OR?V;$)^yg$p3MQaOdE>51!`Y)D8#8=&bTJ%hZyZA{(cd7X~' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
