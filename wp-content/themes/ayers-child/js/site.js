jQuery(document).ready(function($){
    $(window).scroll(function() {
        var mass = Math.max(16, 24-0.05*$(this).scrollTop()) + 'px';
        $('li.menu-item a').css({'font-size': mass, 'line-height': mass});
    });
});