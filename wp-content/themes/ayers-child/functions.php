<?php 

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
    wp_enqueue_script( 'site', get_stylesheet_directory_uri().'/js/site.js', array('jquery'), null, true);

    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

    if ( is_rtl() ) 
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}
function enqueue_calc() {
    wp_enqueue_script( 'jquery1.1', get_stylesheet_directory_uri().'/js/jquery.js', true );
    wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri().'/js/bootstrap.min.js', true );

    wp_enqueue_script( 'calculator', get_stylesheet_directory_uri().'/js/calcs.min.js',array ( 'jquery' ), true );
    wp_enqueue_script( 'autoNumeric', get_stylesheet_directory_uri().'/js/autoNumeric.js',array ( 'jquery' ), true );

}
add_action( 'wp_enqueue_scripts', 'enqueue_calc');


function calculator_borrow_shortcode() {

    ?>
    <div class="panel panel-default"> <!-- /BEGIN CALCULATOR - BORROWING -->
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <form role="form" id="calc-borrowing">
                <div class="panel-body">
                    <div class="row">
                        <div class="vc_col-sm-12 form-group">
                            <label class="control-label">I need a loan to..</label>
                            <select name="purpose" class="calc-input form-control" id="borrowing-purpose">
                                <option value="none">Please select...</option>
                                <option value="buy-first-home">Buy my first home</option>
                                <option value="buy-next-home">Buy my next home</option>
                                <option value="refinance">Refinance my existing loan</option>
                                <option value="buy-investment">Buy an investment property</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Application Type</label>
                            <select name="type" class="calc-input form-control" id="borrowing-type">
                                <option value="single">Single</option>
                                <option value="joint">Joint</option>
                            </select>
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Number of Dependants</label>
                            <select name="dependants" class="calc-input form-control" id="borrowing-dependants">
                                <option value="0">None</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                                <option value="4">Four or more</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Annual Income (before tax)</label>
                            <input name="salary" type="text" class="calc-input form-control format format-currency" data-v-max="500000"  value="75000" id="borrowing-salary">
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Other Income</label>
                            <input name="otherIncome" type="text" class="calc-input form-control format format-currency" data-v-max="500000" value="19000" id="borrowing-otherIncome">
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Estimate my expenses</label>
                            <select name="autoCalc" class="calc-input form-control" id="borrowing-autoCalc">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>

                        <div class="vc_col-sm-6 form-group hide-autoCalc">
                            <label for="personalExpenses" class="control-label ">Personal Expenses</label>
                            <input type="text" name="personalExpenses" class="calc-input format format-currency" data-v-max="500000" value="200" id="borrowing-personalExpenses">
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group hide-autoCalc">
                            <label for="creditCards" class="control-label">Credit Card Repayments</label>
                            <input type="text" name="creditCards" class="calc-input format format-currency" data-v-max="500000" value="200" id="borrowing-creditCards">
                        </div>

                        <div class="vc_col-sm-6 form-group hide-autoCalc">
                            <label for="otherExpenses" class="control-label">Other Monthly Repayment</label>
                            <input type="text" name="otherExpenses" class="calc-input format format-currency" data-v-max="500000" value="200" id="borrowing-otherExpenses">
                        </div>

                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Loan Interest Rate</label>
                            <input name="interestRate" type="text" class="calc-input form-control format format-percentage" data-v-max="50" value="5.00" id="borrowing-interestRate">
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Loan Term</label>
                            <select name="term" class="calc-input form-control" id="borrowing-term">
                                <option value="15">15 Years</option>
                                <option value="25">25 Years</option>
                                <option value="30" selected>30 Years</option>
                            </select>
                        </div>
                    </div>
                    <div class="row result-row">
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">You may be able to borrow up to</label><br/><br/>
                            <span class="calculator__result calc-output format format-currency" data-output="borrowAmount"></span>
                        </div>
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Monthly Repayments at <span class="calc-output format format-percentage" data-output="interestRate"></span></label><br/><br/>
                            <span class="calculator__result calc-output format format-currency" data-output="monthlyRepayment"></span>
                        </div>
                    </div>
                    <div class="row result-row">
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Monthly Repayments at <span class="calc-output format format-percentage" data-output="interestRateBuffered"></span></label><br/><br/>
                            <span class="calculator__result calc-output format format-currency" data-output="monthlyRepaymentsBuffered"></span>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div> <!-- /END CALCULATOR - BORROWING -->
    <?php
}
add_shortcode( 'calculator_borrow_tab', 'calculator_borrow_shortcode' );
function calculator_repayment_shortcode(){
    ?>
    <div class="panel panel-default"> <!-- /BEGIN CALCULATOR - REPAYMENTS -->
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <form role="form" id="calc-repayments">
                <div class="panel-body">
                    <div class="row">
                        <div class="vc_col-sm-12 form-group">
                            <label class="control-label">Loan Amount</label>
                            <input type="text" class="calc-input form-control format format-currency" data-v-max="30000000" name="loanAmount" id="repayment-loanValue" value="250000">
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Term</label>
                            <select name="loanTerm" class="calc-input form-control" id="repayment-loanTerm" >
                                <option value="15">15 Years</option>
                                <option value="20">20 Years</option>
                                <option value="25">25 Years</option>
                                <option value="30" selected>30 Years</option>
                            </select>
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Interest Rate</label>
                            <input type="text" class="calc-input form-control format format-percentage" id="repayment-interestRate" name="interestRate" value="5.00">
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <span style="white-space: nowrap;"><label class="control-label">Repayment Frequency</label></span>
                            <select class="calc-input form-control" name="repaymentFrequency" id="repayment-repaymentFrequency">
                                <option value="monthly" selected>Monthly</option>
                                <option value="fortnightly">Fortnightly</option>
                                <option value="weekly">Weekly</option>
                            </select>
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Repayment Type</label>
                            <select name="repaymentType" class="calc-input form-control" id="repayment-repaymentType">
                                <option value="principalInterest" selected>Principal and Interest</option>
                                <option value="interest">Interest Only</option>
                            </select>
                        </div>
                    </div>
                    <div class="row result-row">
                        <div class="vc_col-sm-6 ">
                            <label class="control-label result-label">Repayment:</label><br/><br/>
                            <span class="calculator__result calc-output">
                                                    <span class="calc-output format format-currency" data-v-max="500000"  data-output="repayment">$1,240</span> <span class="calc-output" data-output="repaymentFrequency">monthly</span>, for <span class="calc-output" data-output="total">25 years</span>
                                                </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- /END CALCULATOR - REPAYMENTS -->
    <?php
}
add_shortcode( 'calculator_repayment_tab', 'calculator_repayment_shortcode');
function calculator_stamp_shortcode(){
    ?>
    <div class="panel panel-default"> <!-- /BEGIN CALCULATOR - STAMP DUTY -->
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <form role="form" id="calc-stampduty">
                <div class="panel-body">
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Property Value</label>
                            <input type="text" class="calc-input form-control format format-currency" id="stampduty-value" value="250000" data-v-max="30000000" name="value">
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">State</label>
                            <select name="state" class="calc-input form-control" id="stampduty-state">
                                <option value="ACT">ACT</option>
                                <option value="NSW" selected>NSW</option>
                                <option value="NT">NT</option>
                                <option value="QLD">QLD</option>
                                <option value="SA">SA</option>
                                <option value="TAS">TAS</option>
                                <option value="VIC">VIC</option>
                                <option value="WA">WA</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">Property Type</label>
                            <select name="type" class="calc-input form-control" id="stampduty-type">
                                <option value="existing" selected>Existing Building</option>
                                <option value="new">Newly Built</option>
                                <option value="offplan">Off the plan</option>
                                <option value="vacant">Vacant Land</option>
                            </select>
                        </div>
                        <div class="vc_col-sm-6 form-group">
                            <label class="control-label">First Home</label>
                            <select name="fho" class="calc-input form-control" id="stampduty-fho">
                                <option value="0" selected>No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="row result-row">
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Registration Fee</label><br><br>
                            <span class="calculator__result calc-output format format-currency" data-v-max="500000" data-output="registration">
                                                    $105
                                                </span>
                        </div>
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Transfer Fee</label><br><br>
                            <span class="calculator__result calc-output format format-currency" data-v-max="500000" data-output="transfer">
                                                    $209
                                                </span>
                        </div>
                    </div>
                    <div class="row result-row">
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Stamp Duty</label><br><br>
                            <span class="calculator__result calc-output format format-currency" data-v-max="500000" data-output="duty">
                                                    $7,240
                                                </span>
                        </div>
                        <div class="vc_col-sm-6">
                            <label class="control-label result-label">Total</label><br><br>
                            <span class="calculator__result calc-output format format-currency" data-v-max="500000"  data-output="total">
                                                    $7,554
                                                </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- /END CALCULATOR - STAMP DUTY -->
    <?php
}
add_shortcode( 'calculator_stamp_tab', 'calculator_stamp_shortcode');
?>